
module.exports = {
    commands: {

        init: {
            description: 'Generates a base Nodecaf project file structure in the current directory',
            args: [ 'APP_NAME' ],
            opts: {
                version: { alias: 'v', description: 'Nodecaf version to be used in the new app (defaults to latest)' },
                conf: { alias: 'c', description: 'Type of config file to be used in the project', value: 'json|yml|toml' },
                bin: { flag: true, description: 'When present will generate a npm binary file to run the app' },
                bare: { flag: true, description: 'When present will generate only js files' },
                path: { alias: 'p', description: 'Project root directory (defaults to app name inside working dir)', value: 'path' },
                mongo: { flag: true, description: 'When present will install and generate code for mongo integration' },
                redis: { flag: true, description: 'When present will install and generate code for redis integration' },
                docker: { description: 'Which Docker Image variant to use in the generated Dockerfile. Defaults to regular image', value: 'node22|slim' }
            },
            action: input => require('./cli/init')(input)
        },

        run: {
            description: 'Executes the Nodecaf app installed in the specified directory',
            opts: {
                conf: { alias: 'c', multi: true, description: 'Conf file path (multiple supported)', value: 'file' },
                reload: { alias: 'r', flag: true, description: 'Whether the app should be reloaded upon code changes' }
            },
            args: 'APP_PATH',
            action: input => require('./cli/run')(input)
        },
    },
    opts: {
        version: { alias: 'v', flag: true, description: 'Output the installed cli version' }
    },
    action: input => require('./cli/version')(input)
};