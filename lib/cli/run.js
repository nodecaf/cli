const path = require('path');
const fs = require('fs').promises;

function loadModule(input){
    return require(path.resolve(input.args[0] ?? process.env.APP_PATH ?? ''));
}

function loadApp(init){
    const app = typeof init == 'function' ? init() : init;

    if(app.constructor.name != 'Nodecaf')
        throw new TypeError('The module does not return a Nodecaf instance');

    if(typeof app.run != 'function')
        throw new Error('Cannot `run` Nodecaf versions below v0.13.2');

    return app;
}

function runApp(input, app){
    return app.run({ conf: input.opts.conf ?? process.env.APP_CONF });
}

async function invalidateModuleTree(parent, comparisonTable, loopControl = {}){
    let anyChanged = false;

    if(parent.filename in loopControl)
        return false;
    loopControl[parent.filename] = true;

    let mtime;
    try{
        // console.log('statting file', parent.filename);
        mtime = (await fs.stat(parent.filename)).mtimeMs;
    }
    catch(_err){
        throw new Error('This dependency file was deletted: ' + parent.filename);
    }

    if(comparisonTable[parent.filename] && mtime != comparisonTable[parent.filename]){
        // console.log(
        //     'this file changed',
        //     parent.filename,
        //     comparisonTable[parent.filename],
        //     mtime
        // );

        anyChanged = true;
        delete require.cache[parent.filename];
    }

    comparisonTable[parent.filename] = mtime;

    for(const c of parent.children)
        if(c.filename.endsWith('.js') && !c.filename.includes('node_modules')){
            const childChanged = await invalidateModuleTree(c, comparisonTable, loopControl);
            if(childChanged){
                delete require.cache[parent.filename];
                anyChanged = true;
            }
        }

    return anyChanged;
}

async function runWithReload(input){
    const context = { reloads: 0 };
    let resolveReload, rejectReload;
    let firstTime = true;
    const reloadTable = {};

    async function reloadApp(){
        try{
            // console.log('loading app');
            const init = loadModule(input);
            const app = loadApp(init);

            const root = module.children.find(c => c.exports == init);

            firstTime && await invalidateModuleTree(root, reloadTable);
            firstTime = false;

            const ri = setInterval(async function(){
                // console.log('interval');

                const hasChanges = await invalidateModuleTree(root, reloadTable);

                if(hasChanges){
                    // console.log('found changes');
                    await app.stop();
                    reloadApp();
                    context.reloads++;
                }
            }, 1000);

            app._oldStop = app.stop;
            app.stop = () => {
                // console.log('cleared interval');
                clearInterval(ri);
                return app._oldStop();
            }

            !firstTime && context.reloads++;
            context.app = app;
            await runApp(input, app);
            // console.log('load finished');
            resolveReload?.();
        }
        catch(err){
            rejectReload?.(err);
        }
        // console.log('prom from inside', context.nextReload);

        context.nextReload = new Promise((resolve, reject) => {
            resolveReload = resolve;
            rejectReload = reject;
        });

        // console.log('prom from inside after reload', context.nextReload);
    }

    await reloadApp();

    return context;
}

module.exports = input => {
    if(input.opts.reload)
        return runWithReload(input);

    const init = loadModule(input);
    const app = loadApp(init);
    return runApp(input, app);
};
