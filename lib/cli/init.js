const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const c = require('colors/safe');
const proc = require('child_process');
const muhb = require('muhb');

const template = name => fs.readFileSync(
    path.resolve(__dirname, '../codegen', name + '.ejs'),
    'utf-8'
);

/* istanbul ignore next */
function log(text){
    if(process.env.NODE_ENV == 'testing')
        return;

    console.log(text);
}

function mkdir(path){
    try{
        fs.mkdirSync(path, { recursive: true });
    }
    catch(e){}
}

function generate(templateName, path, input){
    if(!fs.existsSync(path)){
        let src = ejs.render(template(templateName), input, {});
        src = src.replace(/[\n\r]/g, '\r\n');
        fs.writeFileSync(path, src);
        log('  ' + c.brightGreen('+ ./' + path));
    }
}

/* istanbul ignore next */
let pipeConf = () => process.env.NODE_ENV == 'testing'
    ? 'ignore'
    : [0, 1, 2];

function generateMainFiles(name, input){
    log('\n🔹 Initializing project in ' + c.yellow(path.resolve('.')));
    mkdir('lib');

    generate('main', 'lib/main.js', input);
    generate('routes', 'lib/routes.js', input);
}

function generateMiscFiles(name, input){
    if(input.opts.bare)
        return false;

    log('\n🔹 Generating miscellaneous files');

    input.opts.docker = input.opts.docker
        ? '-' + input.opts.docker
        : '';

    generate('docker', 'Dockerfile', input);
    generate('eslint', '.eslintrc.yml', input);
}

function generatePackageJSON(name, input){
    log('\n📦 Initializing NPM package');

    let pkgJSONPath = 'package.json';
    let pkgInfo = { name, version: '0.0.0', main: 'lib/main.js' };
    let pkgEx = fs.existsSync(pkgJSONPath);
    let curPkg = pkgEx ? JSON.parse(fs.readFileSync(pkgJSONPath, 'utf-8')) : {};
    pkgInfo = { ...pkgInfo, ...curPkg };
    pkgInfo.scripts = {
        lint: 'npx eslint --fix ./lib/**/*.js',
        start: 'npx nodecaf run ./lib/main.js',
        ...pkgInfo.scripts ?? {}
    };

    if(input.opts.bin){
        pkgInfo.bin = pkgInfo.bin || {};
        pkgInfo.bin[name] = pkgInfo.bin[name] || 'bin/' + name + '.js'
    }

    fs.writeFileSync(pkgJSONPath, JSON.stringify(pkgInfo, null, 2));
    log('  ' + c[pkgEx ? 'cyan' : 'brightGreen']('+ ./package.json'));

    if(!pkgInfo.dependencies || !pkgInfo.dependencies.nodecaf)
        setupDependencies(name, input);
}

function setupDependencies(name, input){
    log('\n🔹 Installing dependencies');
    let deps = [ 'nodecaf@' + input.opts.version.join('.') ];
    input.opts.conf && deps.push(input.opts.conf);
    input.opts.mongo && deps.push('nodecaf-mongo');
    input.opts.redis && deps.push('nodecaf-redis');
    proc.execSync('npm i -P ' + deps.join(' '), { stdio: pipeConf() });
}

// Create BIN folder and the run binary.
function generateRunFile(name, input){

    let fp = 'bin/' + name + '.js'

    if(!input.opts.bin || fs.existsSync(fp))
        return false;

    log('\n⚙️ Generating run script (bin)');

    let src = ejs.render(template('run'), input, {});
    mkdir('bin/');
    fs.writeFileSync(fp, src);

    log('  ' + c.brightGreen('+ ./' + fp));
}

function generateGitRepo(name, input){
    if(input.opts.bare)
        return false;

    log('\n🔹 Initializing git repository');
    try{
        proc.execSync('git init', { stdio: pipeConf() });

        /* istanbul ignore next */
        if(!fs.existsSync('.gitignore')){
            fs.writeFileSync('.gitignore', '/node_modules/\r\n');
            log('  ' + c.brightGreen('+ ./.gitignore'));
        }

        /* istanbul ignore next */
        if(!fs.existsSync('.gitattributes')){
            fs.writeFileSync('.gitattributes', '*.js text eol=crlf\r\nbin/*.js text eol=lf\r\n');
            log('  ' + c.brightGreen('+ ./.gitattributes'));
        }
    }
    catch(e){

        /* istanbul ignore next */
        log('❌ ' + c.red('Failed to exec git init command'));
    }
}

async function getTargetVersion(input){
    const { body } = await muhb.get('https://registry.npmjs.org/nodecaf');
    const data = JSON.parse(body);

    if(!input.opts.version){
        input.opts.version = data['dist-tags'].latest.split('.');
        return;
    }

    let [ maj,  min, p ] = input.opts.version.replace('v', '').trim().split('.');
    p = !min || min == 'x' || !p || p == 'x' ? '\\d+' : p;
    min = min == 'x' || !min ? '\\d+' : min;

    const verRegExp = new RegExp([ maj, min, p ].join('\\.'));

    const foundVersions = Object.keys(data.versions)
        .filter(v => !v.includes('-') && verRegExp.test(v))
        .map(v => v.split('.'))
        .filter(v => v[0] == '0' && v[1] > 11);

    if(foundVersions.length < 1)
        throw new Error(`Could not find Nodecaf version '${input.opts.version}'`);

    input.opts.version = foundVersions[0];

    if(foundVersions.length > 1){
        const latestMin = Math.max(...foundVersions.map(v => Number(v[1])));
        const latestPatch = Math.max(...foundVersions.filter(v => Number(v[1]) == latestMin).map(v => v[2]));
        input.opts.version = foundVersions.find(v => v[1] == latestMin && v[2] == latestPatch);
    }
}

module.exports = async input => {

    if(input.opts.conf && !(input.opts.conf in { toml: 1, yaml: 1 })){
        const err = new Error('\'--conf\' must be one of the following: toml, yaml');
        log(c.red('\n' + err.message));
        return err;
    }

    await getTargetVersion(input);

    const name = input.args[0];
    let projDir = path.resolve(input.opts.path || './' + name);
    mkdir(projDir);
    process.chdir(projDir);

    generateMainFiles(name, input);
    generateMiscFiles(name, input);
    generateRunFile(name, input);
    generateGitRepo(name, input);
    generatePackageJSON(name, input);

    log('\n🎉 Successfully initialized project ' + c.yellow(name) + '\n');
}
