
module.exports = input => {

    if(input.opts.version){
        const v = 'Nodecaf CLI v' + require(__dirname + '/../../package.json').version;
        console.log(v);
        return v;
    }

    throw { kind: 'no-action' };

};