/* eslint-env mocha */
const { eclipt } = require('eclipt');
const fs = require('fs').promises;
const assert = require('assert');
const Tempper = require('tempper');

const cli = require('../lib/main');
const nodecaf = (...args) => eclipt('nodecaf', cli, args);

process.env.NODE_ENV = 'testing';
process.env.ENV = 'testing';

describe('CLI: nodecaf', () => {
    var tmp;

    before(function(){
        process.chdir('./test');
        tmp = new Tempper();
    });

    after(function(){
        tmp.clear();
        process.chdir('..');
    });

    describe('nodecaf -v', () => {

        it('Should output current version', function() {
            nodecaf('-v');
        });

        it('Should fail when no command is sent', function() {
            assert(nodecaf().error);
        });

    });

    describe('nodecaf init', () => {
        const fs = require('fs');
        const assertPathExists = p => assert(fs.existsSync(p));

        afterEach(function(){
            tmp.refresh();
        });

        it('Should fail when no name is sent', async function() {
            assert((await nodecaf('init')).error);
        });

        it('Should fail when bad config type is sent', async function() {
            assert(await nodecaf('init', '-c', 'bad', 'test') instanceof Error);
        });

        it('Should generate basic structure files', async function() {
            this.timeout(8000);
            await nodecaf('init', 'test');
            assertPathExists('./lib/main.js');
            assertPathExists('./lib/routes.js');
            assertPathExists('./package.json');
        });

        it('Should generate npm bin file', async function() {
            this.timeout(5000);
            tmp.mkdir('test/lib');
            tmp.addFile('res/t-package.json', './test/package.json');
            tmp.addFile('res/app.min.js', './test/lib/main.js');
            tmp.addFile('res/Dockerfile', './test/Dockerfile');
            tmp.addFile('../.eslintrc.yml', './test/.eslintrc.yml');
            await nodecaf('init', '--bin', 'test');
            let pkgInfo = JSON.parse(fs.readFileSync('package.json', 'utf-8'));
            assert.equal(pkgInfo.bin['test'], 'bin/test.js');
        });

        it('Should target specified directory', async function() {
            this.timeout(5000);
            await nodecaf('init', '-p', './foo', 'test');
            assertPathExists('./package.json');
        });

        it('Should generate conf file if specified', async function() {
            this.timeout(5000);
            await nodecaf('init', '-c', 'toml', 'test');
            let pkgInfo = JSON.parse(fs.readFileSync('package.json', 'utf-8'));
            assert('toml' in pkgInfo.dependencies);
        });

        it('Should install redis if specified', async function() {
            this.timeout(8000);
            await nodecaf('init', '--redis', 'test');
            let pkgInfo = JSON.parse(fs.readFileSync('package.json', 'utf-8'));
            assert('nodecaf-redis' in pkgInfo.dependencies);
        });

        it('Should install mongo if specified', async function() {
            this.timeout(8000);
            await nodecaf('init', '--mongo', 'test');
            let pkgInfo = JSON.parse(fs.readFileSync('package.json', 'utf-8'));
            assert('nodecaf-mongo' in pkgInfo.dependencies);
        });

        it('Should allow selecting docker image variant', async function() {
            this.timeout(8000);
            await nodecaf('init', '--docker', 'slim', 'test');
            const dockerfile = fs.readFileSync('Dockerfile', 'utf-8');
            assert(dockerfile.includes('-slim'));
        });

        it('Should only generate main files', async function() {
            this.timeout(5000);
            await nodecaf('init', '--bare', 'test');
            assert(!fs.existsSync('./Dockerfile'));
        });

        it('Should target specific nodecaf version', async function() {
            this.timeout(5000);
            await nodecaf('init', '-v', '0.12.x', 'test');
            let pkgInfo = JSON.parse(fs.readFileSync('package.json', 'utf-8'));
            assert(pkgInfo.dependencies.nodecaf.includes('0.12.'));
        });

    });

    describe('nodecaf run', () => {
        const { get } = require('muhb');
        global.Nodecaf = require('nodecaf');

        afterEach(function(){
            tmp.refresh();
        });

        it('Should fail when path is not correct', () => {
            assert.throws(() => nodecaf('run', './app'), /Cannot find/);
        });

        it('Should fail when init returns other than App', () => {
            tmp.addFile('res/test-package.json', './app.json');
            assert.throws(() => nodecaf('run', './app.json'), /does not return a Nodecaf/);
        });

        it('Should run the given app server', async () => {
            process.env.APP_PATH = './app.min';
            tmp.addFile('res/test-reload.min.js', './test-reload.min.js');
            tmp.addFile('res/app.min.js', './app.min.js');
            const app = await nodecaf('run');
            let { body } = await get('http://localhost:3478/bar');
            assert.strictEqual(body, 'foo');
            await app.stop();
            app._server.closeAllConnections?.();
        });

        it('Should inject multiple conf files', async () => {
            tmp.addFile('res/test-reload.min.js', './test-reload.min.js');
            tmp.addFile('res/app.min.js', './app.min.js');
            tmp.addFile('res/conf.toml', './conf.toml');
            tmp.addFile('res/test-package.json', './conf.json');
            const app = await nodecaf('run', '-c', './conf.toml', '-c', './conf.json', './app.min');
            let { body } = await get('http://localhost:3478/bar');
            assert.strictEqual(body, 'my-proj');
            await app.stop();
            app._server.closeAllConnections?.();
        });

        it('Should reload changed files', async function(){
            this.timeout(4000);
            process.env.APP_PATH = './app.min';
            tmp.addFile('res/app.min.js', './app.min.js');
            tmp.addFile('res/test-reload.min.js', './test-reload.min.js');
            const ctx = await nodecaf('run', '-r');
            {
                const { body } = await get('http://localhost:3478/reload');
                assert.strictEqual(body, 'foobar');
            }

            await fs.writeFile('./app.min.js', (await fs.readFile('./app.min.js', 'utf8')).replace(/foo/g, 'boo'));
            await ctx.nextReload;

            {
                const { body } = await get('http://localhost:3478/reload');
                assert.strictEqual(body, 'boobar');
            }

            await fs.writeFile('./test-reload.min.js', (await fs.readFile('./test-reload.min.js', 'utf8')).replace(/bar/g, 'baz'));
            await ctx.nextReload;

            {
                const { body } = await get('http://localhost:3478/reload');
                assert.strictEqual(body, 'boobaz');
            }

            await ctx.app.stop();
        });

    });

});
