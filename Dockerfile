
# Prepare CLI Files
FROM node:16-alpine
WORKDIR /tmp
COPY bin /tmp/bin
COPY lib /tmp/lib
RUN npm i -P --no-save eclipt nodecaf

# Run guest app
FROM mhart/alpine-node:slim-16

ENV NODE_ENV production

WORKDIR /app

ENTRYPOINT ["node", "/cli/bin/nodecaf.js", "run"]

COPY --from=0 /tmp /cli

RUN addgroup -g 2000 -S nodecaf && \
    adduser -u 2000 -S nodecaf -G nodecaf && \
    chown nodecaf:nodecaf /app

USER nodecaf
